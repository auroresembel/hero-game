## 🚀 Instructions

1.  **Installez le repository.**

    ```sh
    # clonez le repository
    git clone git@gitlab.com:simplon-cahors-promo2/apprenants/exercices/javascript-decouverte-poo.git
    ```

    ```sh
    # Puis créez une branche à votre nom :
    git checkout -b <monnom>
    ```

1.  **Installez ESlint.**

    Installez l'extension ESLint dans votre éditeur de texte.

1.  **Start developing.**

    ```sh
    npm install
    parcel index.html
    ```

